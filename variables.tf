variable "region" {
  default     = "us-east-1"
  description = "AWS Region"
}

variable "cluster_name" {
  default = "EKS-Cluster"
}
variable "docker-image" {
  type        = string
  description = "name of the docker image to deploy"
  default     = "bjgomes/rhombus:latest"
}
variable "app" {
  type        = string
  description = "Name of application"
  default     = "cicd-rhombus"
}
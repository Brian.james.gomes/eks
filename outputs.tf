output "cluster_endpoint" {
  description = "Endpoint for EKS control plane"
  value       = module.eks.cluster_endpoint
}
output "cluster_security_group_id" {
  description = "Security group ids attahed to the clusters"
  value       = module.eks.cluster_security_group_id
}
output "kubectl_config" {
  description = "kubectl config as generated by EKS"
  value       = module.eks.kubeconfig
}
output "config_map_aws_auth" {
  description = "A kubernetes configuration to authenticate to this eks cluster"
  value       = module.eks.config_map_aws_auth
}
output "region" {
  description = "AWS Region"
  value       = var.region
}